"""Actev module: system-setup

Actev modules are used to parse actev commands in order to get arguments
before calling associated entry point methods to execute systems.

Warning: this file should not be modified: see src/entry_points to add your
source code.
"""

import logging

from diva_evaluation_cli.bin.commands.actev_command import ActevCommand
from diva_evaluation_cli.bin.private_src.entry_points.actev_clear_logs import \
    entry_point


class ActevClearLogs(ActevCommand):
    """Delete temporary files as resource and status monitoring files.
    """
    def __init__(self):
        super(ActevClearLogs, self).__init__('clear-logs', entry_point)

    def cli_parser(self, arg_parser):
        """Configure the description and the arguments (positional and
        optional) to parse.

        Args:
            arg_parser(:obj:`ArgParser`): Python arg parser to describe how to
                parse the command

        """
        arg_parser.description = "Delete temporary files as resource and " \
                                 "status monitoring files"
        arg_parser.set_defaults(func=ActevClearLogs.command, object=self)
