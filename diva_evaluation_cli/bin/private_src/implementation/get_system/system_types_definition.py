"""Get system module: system types definition

Dictionary of system types:
    * key: name of the system type
    * value: command to download the system

"""

from diva_evaluation_cli.bin.commands.actev_get_system_subcommands.\
    git_command import ActevGetSystemGit
from diva_evaluation_cli.bin.commands.actev_get_system_subcommands.\
    archive_command import ActevGetSystemArchive


system_types = {
    'git': ActevGetSystemGit,
    'archive': ActevGetSystemArchive
}
