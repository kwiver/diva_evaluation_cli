#!/bin/bash

#####################################################
# Command Line Interface actev: installation script #
#####################################################

CURRENT_DIR=`pwd`
cd "$(dirname "$0")"

# Determine if python3 is running in a virtual environment
python3 private_src/implementation/utils/is_virtual_env.py
EXIT_STATUS=$?

cd "${CURRENT_DIR}"
if [ $EXIT_STATUS -ne 0 ];then

  # Check that .local/bin path is in $PATH
  echo $PATH | grep /.local/bin > /dev/null
  EXIT_STATUS=$?

  if [ $EXIT_STATUS -ne 0 ];then
    echo "Please add ~/.local/bin to your PATH  before running the script:"
    echo "export PATH=\"${PATH}:${HOME}/.local/bin\""
    exit 1
  fi

  # Install using python3
  options='--user'
else
  # Install using venv python
  options=''
fi

cd "$(dirname "$0")"

sudo apt-get install python3-pip -y
sudo apt-get install python3-dev -y

python3 -m pip install setuptools $options
python3 -m pip install -r ../../requirements.txt $options
python3 -m pip install -e ../../. -U $options

# Install dependencies (e.g. ActEV scorer) contained in `install.sh` scripts
# if '--all' is passed as an argument
if [ ! -z "$1" ] && [ "$1" == "--all" ];then
    installation_scripts=`find private_src | grep "install.sh"`
    for install_script in $installation_scripts; do
        echo "Installation in progress: $install_script"
        ./$install_script
	echo "Installation done: $install_script"
    done
fi

