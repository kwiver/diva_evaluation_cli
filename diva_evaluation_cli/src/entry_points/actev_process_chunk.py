"""Entry point module: process-chunk

Implements the entry-point by using Python or any other languages.
"""

import os


def entry_point(chunk_id, system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Process a chunk.

    Args:
        chunk_id (int): Chunk id
        system_cache_dir (str): Path to system cache directory

    """
    script_path = os.path.join(os.path.dirname(__file__), '../implementation/process_chunk.sh')
    status = os.system("{0} {1}".format(script_path, chunk_id))
    if status != 0:
        raise Exception("Processing {0} failed".format(chunk_id))
