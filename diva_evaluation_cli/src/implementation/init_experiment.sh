#!/bin/bash

cd "$(dirname "$0")"

FILE_INDEX=$1
ACTIVITY_INDEX=$2
CHUNK=$3
VIDEO_LOCATION=$4
SYSTEM_CACHE_DIRECTORY=$5

# Start the rc3d container
nvidia-docker  run -itd --name rc3d \
  --entrypoint bash \
  -v ${VIDEO_LOCATION}:/data/diva/frames \
  -v ${SYSTEM_CACHE_DIRECTORY}:/data/diva/system-cache \
  gitlab.kitware.com:4567/kwiver/r-c3d/framework-cli:meva-04-30-2020-combined

nvidia-docker exec rc3d mkdir -p /diva/nist-json
nvidia-docker cp ${FILE_INDEX} rc3d:/diva/nist-json/file-index.json
nvidia-docker cp ${ACTIVITY_INDEX} rc3d:/diva/nist-json/activity-index.json
nvidia-docker cp ${CHUNK} rc3d:/diva/nist-json/chunk.json
