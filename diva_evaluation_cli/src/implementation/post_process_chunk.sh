#!/bin/bash

CHUNK_ID=$1

nvidia-docker start rc3d

nvidia-docker exec -it rc3d /bin/bash -c "merge_videos.py --chunk-id=${CHUNK_ID} --chunk-json=/diva/nist-json/chunk.json --video-root /data/diva/system-cache --out-chunk-root /data/diva/system-cache"
