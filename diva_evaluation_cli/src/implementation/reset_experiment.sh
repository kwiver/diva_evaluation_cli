#!/bin/bash

nvidia-docker start rc3d

nvidia-docker exec -it rc3d /bin/bash -c "cleanup_experiment.py --chunk-json /diva/nist-json/chunk.json --cache-dir /data/diva/system-cache"
sleep 1
nvidia-docker stop rc3d
sleep 1
nvidia-docker rm rc3d


