#!/bin/bash

CHUNK_ID=$1

nvidia-docker start rc3d

nvidia-docker exec -it rc3d /bin/bash -c "cleanup_chunk.py --chunk-id=${CHUNK_ID} --chunk-json=/diva/nist-json/chunk.json --cache-dir /data/diva/system-cache"
