#!/bin/bash

CHUNK_ID=$1

nvidia-docker start rc3d

nvidia-docker exec -it rc3d /bin/bash -c "python3 /R-C3D/cli-helpers/chunk_runner.py --chunk-id=${CHUNK_ID} --chunk-json=/diva/nist-json/chunk.json --experiment-root=/data/diva/system-cache --out-video-root=/data/diva/system-cache --activity-index=/diva/nist-json/activity-index.json --gpus 0 1 2 3 --stride 8"
