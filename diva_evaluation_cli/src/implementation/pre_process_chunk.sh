#!/bin/bash

CHUNK_ID=$1

nvidia-docker start rc3d

nvidia-docker exec -it rc3d /bin/bash -c "generate_experiments.py --chunk-id=${CHUNK_ID} --chunk-json=/diva/nist-json/chunk.json --data-root=/data/diva/frames --experiment-root /data/diva/system-cache --file-index=/diva/nist-json/file-index.json --use-videos"
