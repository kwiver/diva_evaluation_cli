# actev get-system

## Description

Downloads a credentialed, web-accessible content into `/src`.

The command contains the following subcommands:

## get-system git
### Description

Clones a git repository

### Parameters

| Name      | Id | Required        | Definition                 |
|-----------|----|-----------------|----------------------------|
| url       | u  | True        | url to get the system      |
| location  | l  | False       | path to store the system   |
| user      | U  | False       | username to access the url |
| password  | p  | False       | password to access the url |
| token     | t  | False       | token to access the url    |
| name      | n  | False       | name to give to the system |
| sha       | s  | False       | commit SHA or tag to checkout |

### Usage

Generic command:

```
actev get-system git -u <git repository>
```

With username and password: 

```
actev get-system git -u <git repository url> -U <username> -p <password>
```

With a token: 

```
actev get-system git -u <git repository url> -t <token>
```

Store the system in a specific directory:

```
actev get-system git -u <git repository url> ... -l <location>
```

Store the system in a specific directory:

```
actev get-system git -u <git repository url> ... -l <location> -n <directory name>
```

:information_source: You can also directly add your credentials inside the url.

:warning: if your password or token starts with *-* as in this example: `-9r45ijFo0`, you should write `--password=-9r45ijFo0` instead of `-p -9r45ijFo0`. Otherwise, the value will be interpreted as an argument to parse.


Example:

```
actev get-system git -u https://gitlab.kitware.com/actev/diva_evaluation_cli.git -l /tmp
```

Switching on a specific commit after cloning:

```
actev get-system git -u <url> -s <commit SHA or tag>
```

:warning: You can also use this feature with tags. However, be careful your project does not have a branch with the same name than your tag, otherwise the checkout will happen on the branch and not the tag. The correct format for tags is `--sha tags/<tag>`.

## get-system archive

### Description

Downloads a web resources as a zip/tar file

### Parameters

| Name      | Id | Required        | Definition                 |
|-----------|----|-----------------|----------------------------|
| url       | u  | True            | url to get the system      |
| location  | l  | False           | path to store the system   |
| user      | U  | False           | username to access the url |
| password  | p  | False           | password to access the url |
| token     | t  | False           | token to access the url    |

### Usage

Generic command:

```
actev get-system archive -u <web resource url>
```

With username and password: 

```
actev get-system archive -u <web resource url> -U <username> -p <password>
```

With a token: 

```
actev get-system archive -u <web resource url> -t <token>
```

Store the system in a specific directory:

```
actev get-system archive -u <web resource url> ... -l <location>
```

:information_source: You can also directly add your credentials inside the url.

:warning: if your password or token starts with *-* as in this example: `-9r45ijFo0`, you should write `--password=-9r45ijFo0` instead of `-p -9r45ijFo0`. Otherwise, the value will be interpreted as an argument to parse.


Example:

```
actev get-system archive -u https://path/to/file.tgz -l /tmp
```
