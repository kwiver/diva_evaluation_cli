# Adapting diva_evalution_cli for R-C3D based SDL Submission
This fork of [diva_evalaution_cli](https://gitlab.kitware.com/actev/diva_evaluation_cli) is used to submit R-C3D to [ActEV SDL challenge](https://actev.nist.gov/sdl) and provide documentation for the R-C3D based implementation of diva_evaluation_cli. We assume that you have successfully installed and verified actev based on the following [instructions](https://gitlab.kitware.com/actev/diva_evaluation_cli/blob/master/README.md). 

# Introduction
R-C3D uses a prebuilt docker container to implement the different methods present in `src/entry_points`. The `src/entry_points` are thin wrappers to invoke the implementation in `src/implementation`. The implementations are primarily used to access the docker container containing a fork of [R-C3D](https://gitlab.kitware.com/kwiver/R-C3D/tree/kitware/master) along with [diva-framework](https://pypi.org/project/diva-framework/) and [kwiver](https://pypi.org/project/kwiver/). The details about the container and adpating R-C3D to the framework are present in [Kitware-README](https://gitlab.kitware.com/kwiver/R-C3D/blob/dev/modify-documentation/Kitware-README.md) in R-C3D.

# Actev Commands

## system-setup
Runs `src/implementation/setup.sh` to install [ffmpeg](https://www.ffmpeg.org/), [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [nvidia-drivers](https://www.nvidia.com/Download/index.aspx) and [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) on a server. It pulls [framework-cli:multi-gpu](gitlab.kitware.com:4567/kwiver/r-c3d/framework-cli:multi-gpu) container.

## design-chunks
Calls `design_chunk` in `src/implementation/design_chunk.py` to create chunk.json and store it in a location specified by `output` in the entrypoint.

## experiment-init
Runs `src/implementation/init_experiment.sh` to run the docker container. It mounts `video_location` and `system_cache_directory` on the docker container at `/data/diva/frames` and `/data/diva/system-cache` respectively. Additionally it copies the file-index.json, activity-index.json and chunk.json to `/diva/nist-json` in the container.

## pre-process-chunk
Runs `src/implementation/pre_process_chunk.sh` to generate a configuration file for `diva_experiment_process` for every video in a chunk. The configuration files are generated using `generate_experiment.py` provided by diva-framework. The configuration file provides the location for video along with the algorithm that would be used to read the video. RC3D uses ffmpeg based implementation of `video_input` provided by kwiver to read the videos.

## process-chunk
Runs `src/implementation/process_chunk.sh` to execute `cli-helpers/chunk_runner.py` in the container. The chunk runner iterates through the videos in a chunk and runs rc3d pipeline on each video in the chunk. For every video, the pipeline uses the configuration file produced in the previous step along with activity-index.json to run R-C3D. The output is saved in the `system_cache_directory` as a NIST compliant json file for post processing.

Note: The json files produced in this stage contains activities associated with a single video.

## post-process-chunk
Runs `src/implementation/post_process_chunk.sh` to merge json files generated for individual videos. The resulting json file is saved in `system_cache_directory` with the name chunk-<chunk-id>.json. The new json file represents the activities present in a chunk. The file is generated using `merge-videos` provided by diva-framework.

Note: The json files produced in this stage contains activities associated with a single chunk.

## merge-chunks
Calls `merge_chunks` in `src/implementation/merge_chunks.py` merges the chunk based json to create a single json file.

## reset-experiment
Runs `src/implementation/reset_experiment.sh` to remove all intermediate chunk-<chunk-id>.json files.

